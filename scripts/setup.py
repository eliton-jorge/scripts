from cx_Freeze import setup, Executable
from datetime import datetime
import os
import smtplib
import subprocess
import sys
import time


base = None

if sys.platform == "win32":
    base = '--base-name=Win32GUI'

executables = [
        Executable("tarefa.pyw", base=base)
]

buildOptions = dict(
        packages = [],
        includes = [],
        include_files = [],
        excludes = []
)

setup(
    name = "verifica painel",
    version = "1.0",
    description = "verificacao painel",
    options = dict(build_exe = buildOptions),
    executables = executables
 )
